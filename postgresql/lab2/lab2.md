
# Lab 2 

1. Login in PostgreSQL as an admin

```console
sudo -u postgres psql
```

2. Delete all the databases and users created in lab 1

List the users:
```postgres=# \du```

List the databases:
```postgres=# \du```

3. With an SQL query, create a new user whose name will be your system username ($USER)

``` sql
CREATE USER 'user' LOGIN;
```

4. Redefine the newly created user password using an internal command from PostgreSQL

```
\password 'user'
```

5. Redefine the newly created user password this time using an SQL query

```sql
ALTER USER 'user' WITH PASSWORD 'password'
```

6. Create a new role called ```dbcreator``` who doesn't have login privileges but can create new databases in the cluster

```sql
CREATE ROLE dbcreator WITH CREATEDB
```


7. Create a new role called ```rolecreator``` who doesn't have login privileges but can create new roles in the cluster

```sql
CREATE ROLE rolecreator WITH CREATEROLE;
```

8. Add the roles ```dbcreator``` and ```rolecreator``` to the user ```'user'```

```sql
GRANT dbcreator, rolecreator TO 'user';
```

9. List the roles using a psql command

```postgres
postgres=# \dg
```

10. List the roles using a SQL query

```sql
SELECT rolname FROM pg_roles;
```

11. 

```sql
SELECT * from pg_auth_members;
```

13. What's the difference between ```INHERIT``` and ```NOINHERIT``` ?

If the session user role has the INHERIT attribute, then it automatically has all the privileges of every role that it could SET ROLE to; in this case SET ROLE effectively drops all the privileges assigned directly to the session user and to the other roles it is a member of, leaving only the privileges available to the named role. On the other hand, if the session user role has the NOINHERIT attribute, SET ROLE drops the privileges assigned directly to the session user and instead acquires the privileges available to the named role.

14. 

``` console
psql -d postgres -h localhost -U simazenoux
```

```
nano /etc/postgresql/15/main/pg_hba.conf
```

and we replace

```
# "local" is for Unix domain socket connections only
local   all             all                                     peer
```

by 
```
# "local" is for Unix domain socket connections only
local   all             all                                     scram-sha-256
```
and we now can connect ourself using the command 

```
psql -d postgres -h localhost -U simazenoux
```

16. Log in your PostgreSQL instance with your personnal account.

```
psql
```


17. Create a database.


```SQL
CREATE DATABASE tmp;
```

18. Log in this newly created database

```console
psql -d tmp -h localhost -U simazenoux
```

19. Create a table with at least two columns;

```SQL
CREATE TABLE tab ( col1 INT, col2 INT);

INSERT INTO tab VALUES (1,2);
```

20. 

```SQL
CREATE EXTENSION pageinspect;
SELECT * FROM heap_page_items(get_raw_page('tab',0));
```

21. 

```SQL
SELECT ctid, xmin, xmax FROM tab;
```
