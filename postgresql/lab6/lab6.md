
# Lab 6 

1. Démarrez votre machine virtuelle personnelle, et l’instance PostgreSQL utilisée lors
des TP précédents.

```console
sudo systemctl start postgresql
```


2. Vérifiez que l’instance utilise le port par défaut, à savoir, le 5432. Vous pouvez pour cela utiliser la commande pg_lsclusters. Si besoin, nettoyer les instances (autres versions) inutiles, reconfigurez votre instance principale, et redémarrez la, afin qu’elle utilise le port 5432.

```console
pg_lsclusters 
```
Seule l'instance 15 main est lancée, on peut donc continer


3. Vérifiez que le port 5433 n’est actuellement pas utilisé (par aucun processus et aucun utilisateur).

```console
sudo netstat -tulpn |grep 5433
```
Le résultat est vide, on peut continuer.


4. Créez un nouvel utilisateur système, que vous nommerez repli.

```console
sudo adduser repli 
```


5. Créez un nouvel utilisateur PostgreSQL, que vous nommerez repli. Cet utilisateur ne doit avoir que le droit d’initier une connexion de replication.

On se connecte à l'instance en tant qu'utilisatuer postgres
```console
sudo -u postgres psql
```

Et on crée le nouvel utilisateur à l'aide de la commande SQL suivante :
```SQL
CREATE USER repli REPLICATION;
```

6. Modifiez le fichier pg_hba.conf de votre instance principale, pour faire en sorte que seul cet utilisateur et le super-utilisateur postgres puisse initier une connexion locale de replication via la méthode peer. Demandez à l’instance courante de prendre en compte ce changement, et vérifiez.

On édite le fichier 
```console
sudo nano /etc/postgresql/15/main/pg_hba.conf
```

Et on remplace dans la ligne suivante $all$ par $repli$:
```
local replication postgres,repli  peer
```

On redémarre l'instance :
```
sudo systemctl restart postgresql
```

Pour vérifier que la configuration soit bien mise à jour :
```console
sudo -u postgres psql
```
Puis
```SQL
SELECT * FROM pg_hba_file_rules;
```

7. Regardez l’aide de l’utilitaire pg_basebackup, pour y trouver son rôle, la signi-
fication des options -R, -P, -c fast, ainsi que celle qui permettra d’indiquer la
destination de la sauvegarde à réaliser.


Pour afficher la doc:
```console
man pg_basebackup
```

$-R$ : 
```
-R
       --write-recovery-conf
           Creates a standby.signal

           file and appends connection settings to the postgresql.auto.conf file in the target directory (or within
           the base archive file when using tar format). This eases setting up a standby server using the results of
           the backup.

           The postgresql.auto.conf file will record the connection settings and, if specified, the replication slot
           that pg_basebackup is using, so that streaming replication will use the same settings later on.
```

$-P$ : 
```
-R
       --write-recovery-conf
           Creates a standby.signal

           file and appends connection settings to the postgresql.auto.conf file in the target directory (or within
           the base archive file when using tar format). This eases setting up a standby server using the results of
           the backup.

           The postgresql.auto.conf file will record the connection settings and, if specified, the replication slot
           that pg_basebackup is using, so that streaming replication will use the same settings later on.
```

$-c \; fast$ :
```
-c {fast|spread}
       --checkpoint={fast|spread}
           Sets checkpoint mode to fast (immediate) or spread (the default) (see Section 26.3.3).
```

Pour avoir la destination : 
$-D$
```
-D directory
       --pgdata=directory
           Sets the target directory to write the output to.  pg_basebackup will create this directory (and any
           missing parent directories) if it does not exist. If it already exists, it must be empty.

           When the backup is in tar format, the target directory may be specified as - (dash), causing the tar file
           to be written to stdout.

           This option is required.
```

8. Utilisez toutes ses options pour réaliser une sauvegarde à chaud du cluster, dans un
répertoire repli, frère du répertoire contenant les fichiers de l’instance principale.

```
sudo -u postgres pg_basebackup -D /var/lib/postgresql/15/repli -c fast -P -R
```

9. Listez les fichiers du répertoire contenant la sauvegarde

```console
sudo ls /var/lib/postgresql/15/repli
```

10. Regardez le manuel de la commande pg_createcluster, puis créez un nouveau cluster, qui appartiendra à l’utilisateur repli, utilisera le port 5433, emploiera la version 15 du serveur PostgreSQL et s’appellera repli.

```console
man pg_createcluster
```

On ajoute l'utilisateur système repli au groupe postgres.
```
sudo usermod -aG postgres repli
```

```console
sudo pg_createcluster 15 replie --user repli --port 5433
```

On peut vérifier la création à l'aide de la commande suivante 
```console
pg_lsclusters
```

