# Lab 1 : Installation, startup, config and connexion

1. Login into your virtual machine, how can you obtain the os info ?

```
cat /etc/os-release
```


2. Is there already a paquet called postresql available in your package manager ?

```console
apt search ^postgresql$
apt show postgresql
```


3. Add the official PosgreSQL repository. Which versions are now available ? Which version is now choosen by default ?

```console
apt show postgresql -a
```


4. Install the last version of PostgreSQL. Which version has been installed ?

```console
psql --version
```

5. Once installed, look at the system configuration used.

```
pg_config
```


6. A systemd service has been created for the postgresql server.
    1. Start the service
    2. Check the service status
    3. Disable the automatic start of the service on startup

``` console
systemctl start postgresql
systemctl status postgresql
systemctl disable postgresql
```



7. What is the name of the service started by the postgres service?

``` console
systemctl list-units postgresql@*
```

8. Check the status of the instance (systemctl status), and find the following useful information the following useful information:
    1. What is the PID of the main postgresql process?
    2. What is the memory footprint of the server?
    3. How many processes have been started for the postgresql server?
    4. Which postgresql configuration file was used?

By using the ```systemctl status postgresql@15-main.service``` 



9. To which user belongs the instance processes?

By using ```ps u $POSTGRES_PID```, we now know that the instance processes belongs to the user postgres.


10. Look at the contents of the configuration file (see question 8.4), and answer the following questions:
    1. Where are the data files located?
    2. Where is the ```hba``` authentication file?
    3. Where is the ```ident``` identification file file?
    4. Does the server listen to connections on the network?
    5. What is the maximum number of (parallel) connections allowed by the instance?
    6. How much shared buffer space is allocated?
    7. What is the waiting time of a lock before launching a deadlock detection deadlock detection procedure?


```
data_directory = '/var/lib/postgresql/15/main'
hba_file = '/etc/postgresql/15/main/pg_hba.conf'
ident_file = '/etc/postgresql/15/main/pg_ident.conf'

max_connections = 100
shared_buffers = 128MB
default_text_search_config = 'pg_catalog.french'
```


The server is set up to be accessible only by localhost by default




11. Where are located the data files and WAL files of the instance by default?

base/
pg_wal/

12. Check that ```psql``` is installed on your machine.

``` console
psql --version
```

13. From your personal session, try to connect:
    1. As yourself
    2. As the postgres user
    3. As the postgres user via localhost
    4. As the postgres system user 

``` console
psql
psql -U postgres
psql -U postgres -h localhost
sudo -u postgres psql
```

14. Log in using the winning method and view the help.

```console
postgres=# help
```

15. Define a new password for the user postgres.

```console
postgres=# \password postgres
```

16. Look at the documentation of the createuser shell tool.

```console
createuser --help
```

17. Using the ```createuser``` command, create a user named as your personal system user, giving it the right to log in and specifying a password (which you will remember). Which SQL command was sent?

```console
sudo -u postgres createuser --login --password -e simazenoux
```
The resulting SQL query from this command is 
``` SQL
SELECT pg_catalog.set_config('search_path', '', false);
CREATE ROLE simazenoux NOSUPERUSER NOCREATEDB NOCREATEROLE INHERIT LOGIN;
```

18. Try to connect to this user from your system session. What happens? Can you connect to a particular database?

``` console
psql
```

When trying to login, an error appears saying that the "simazenoux" database doesn't exist.

``` console
psql -d postgres
```

19. With the shell tool ```createdb```, create a database named like your personal user and which belongs to your personal user, indicating a description. Which SQL command has been sent?

``` console
sudo -u postgres createdb --owner simazenoux -e simazenoux "La base de Simon !"
```

The resulting SQL queries from this command are:

``` SQL
SELECT pg_catalog.set_config('search_path', '', false);
CREATE DATABASE simazenoux OWNER simazenoux;
COMMENT ON DATABASE simazenoux IS 'La base de Simon !';
```

20. Log in from your system session to the newly created database.

``` console
psql
```