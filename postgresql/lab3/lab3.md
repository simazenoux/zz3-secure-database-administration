# Lab 3

1. List the privileges of your user.

```
simazenoux=> \du
```

2. Create a table named ```tbl```

```SQL
CREATE TABLE tbl (col1 INT, col2 INT);

INSERT INTO tbl VALUES (0,200), (1, 400), (2, 800);
```

3. 

```SQL
CREATE ROLE datareader;
GRAND SELECT ON tbl TO datareader;
```


4. 

```SQL
CREATE ROLE dataincwriter;
GRANT INSERT ON tbl TO dataincwriter;
```


5. 

```SQL
Create ROLE datadecwriter NOINHERIT;
GRANT DELETE ON tbl TO datadecwriter;
```


6. 

```SQL
CREATE USER positif;
CREATE USER negatif;
GRANT dataincwriter TO positif;
GRANT datadecwriter TO nefatif;
ALTER USER positif WITH PASSWORD 'coucou';
ALTER USER negatif WITH PASSWORD 'coucou';
```


7. 

```console
psql (d postgres - h localhost - U negatif
psql -d postgres -h 
```


8. 

| Action      | User positif | User negatif  |
| ----------- | ------------ | ------------- |
| Read        | ✅           | ✅            |
| Insert      | ✅           | ❌            |
| Delete      | ❌           | ✅            |
| Update      | ❌           | ❌            |



