# Lab 5

1. 

Pour vérifier le statut de l'instance PostgreSQL :
```console
systemctl status postgresql
```

Pour démarrer les différents clusters : 
```console
sudo systemctl start postgresql
```

On peut lister les différentes instances lancés par le service à l'aide de la commande suivante :
```console
systemctl list-units postgresql@*
```

On remarque alors que une instance a été lancée : ```postgresql@15-main```

2. 

On peut connaître les processus de l'instance à l'aide de la commande suivante :
```console
systemctl status postgresql@15-main
```
On remarque alors que, en plus de l'instance en elle même (PID: 1673), 5 autres processus ont été lancés par l'instance : 
- checkpointer (PID: 1674)
- background writer (PID: 1675)
- walwriter (PID: 1677)
- autovacuum launcher (PID: 1678)
- logical repication launcher (PID: 1679)



3. 

Le chemin du fichier de configuration de l'instance est ```/etc/postgresql/15/main/postgresql.conf```. On peut être certain que c'est bien ce fichier car nous pouvons voir parmi les données affichées par la commande ```systemctl status postgresql@15-main```, on voit que ce fichier à été donné en paramètre comme ```config_file``` lors de l'instanciation de l'instance.

Il est alors possible de récupérer le fichier de configuration à l'aide de la commande ```scp```:
```console
scp simazenoux@vm-etu-simazenoux.local.isima.fr:/etc/postgresql/15/main/postgresql.conf postgresql-q3.conf
```


4. 

Dans le fichier ```/etc/postgresql/15/main/postgresql.conf```, on remarque les lignes suivante :
```
#listen_addresses = 'localhost'		# what IP address(es) to listen on;
					# comma-separated list of addresses;
					# defaults to 'localhost'; use '*' for all
					# (change requires restart)
```

On peut alors conclure que l'instance écoute le réseau local (```localhost```) mais pas les connexions distantes. Pour activer les réseaux distants, modifie le fichier ainsi : 
```
listen_addresses = '*'		# what IP address(es) to listen on;
					# comma-separated list of addresses;
					# defaults to 'localhost'; use '*' for all
					# (change requires restart)
```

Pour que le changement soit pris en compte, on utilise la commande suivante :
```console
sudo systemctl restart postgresql@15-main
```

Enfin, on copie notre fichier nouvellement modifié :
```console
scp simazenoux@vm-etu-simazenoux.local.isima.fr:/etc/postgresql/15/main/postgresql.conf postgresql-q4.conf
```



5. 

Pour connaître le fichier pris en compte, il suffit de regarder la valeur associé au paramètre ```hba_file``` du fichier de configuration (c.f. question 3). On constate alors que le fichier utilisé est le ```/etc/postgresql/15/main/pg_hba.conf```.

Le fichier de configuration de connexion de l'instance postgres est situé sur le chemin suivant : ```/etc/postgresql/15/main/pg_hba.conf```. On observe alors que l'instance postgres écoute les connexions distantes (```host```) pour le réseau local pour les protocoles IPv4 et IPv6 (127.0.0.1/32 et ::1/128 étant respectivement les adresses de loopback).

```
# TYPE  DATABASE        USER            ADDRESS                 METHOD
local   all             all                                     scram-sha-256
host    all             all             127.0.0.1/32            scram-sha-256
host    all             all             ::1/128                 scram-sha-256
```


On copie alors les fichiers de configuration sur notre instance locale.
```console
ssh simazenoux@vm-etu-simazenoux.local.isima.fr "sudo cat /etc/postgresql/15/main/pg_hba.conf" > pg_hba-q5.conf
```


6. 

Pour donner accès à notre instance, on ajoute alors la lighe suivante au fichier ```etc/postgresql/15/main/pg_hba.conf```:
```
# TYPE  DATABASE        USER            ADDRESS                 METHOD
host    sameuser        all             10.16.32.0/20           scram-sha-256
```

Il est alors possible de faire en sorte de recharger le fichier de configurationà l'aide de la commande SQL ```SELECT pg_reload_conf()``` avec l'utilisateur postgres.
```console
sudo -u postgres psql -c "SELECT pg_reload_conf()"
```

On copie alors les fichiers de configuration sur notre instance locale.
```console
ssh simazenoux@vm-etu-simazenoux.local.isima.fr "sudo cat /etc/postgresql/15/main/pg_hba.conf" > pg_hba-q6.conf
```


7. 

Pour se connecter à l'instance en tant que super-utilisateur, il faut se connecter en tant qu’utilisateur système postgres avec la commande suivante :
```console
sudo -u postgres psql
```


8. 

Depuis l'instance, on peut créer un utilisateur à l'aide d'une simple commande SQL:
```SQL
CREATE USER dbtp5 LOGIN PASSWORD 'password';
```


9. 

```SQL
CREATE DATABASE dbtp5 OWNER dbtp5;
```

10. 

On utilise la commande suivante :
```SQL
SELECT * FROM pg_hba_file_rules WHERE database = sameuser;
```

et on obtient alors le résultat suivant : 
```
 line_number | type  |   database    | user_name  |  address   |                 netmask                 |  auth_method  | options | error 
-------------+-------+---------------+------------+------------+-----------------------------------------+---------------+---------+-------
          90 | local | {all}         | {postgres} |            |                                         | peer          |         | 
          95 | local | {all}         | {all}      |            |                                         | scram-sha-256 |         | 
          97 | host  | {all}         | {all}      | 127.0.0.1  | 255.255.255.255                         | scram-sha-256 |         | 
          98 | host  | {sameuser}    | {all}      | 10.16.32.0 | 255.255.240.0                           | scram-sha-256 |         | 
         100 | host  | {all}         | {all}      | ::1        | ffff:ffff:ffff:ffff:ffff:ffff:ffff:ffff | scram-sha-256 |         | 
         103 | local | {replication} | {all}      |            |                                         | peer          |         | 
         104 | host  | {replication} | {all}      | 127.0.0.1  | 255.255.255.255                         | scram-sha-256 |         | 
         105 | host  | {replication} | {all}      | ::1        | ffff:ffff:ffff:ffff:ffff:ffff:ffff:ffff | scram-sha-256 |         | 
```

On constate alors qu'un utilisateur peut se connecter à une database qui porte son nom sur le réseau d'adresse 10.16.32.0/20 par la méthode scram-sha-256.


11. 

Le fichier json est le suivant : 
```json
{
	"host":
	{
		"ip": "10.16.36.113",
		"name": "vm-etu-simazenoux.local.isima.fr"
	},
	"db": "dbtp5",
	"user": "dbtp5",
	"password": "password"
}
```

12. 

On ajoute la ligne suivante au fichier ```etc/postgresql/15/main/pg_hba.conf```:
```
local   all             simazenoux                              ident
```