# Lab 3

## 1 - Gestion des utilisateurs

### Créer un utilisateur pour vous. Le login sera votre nom. Assurez-vous également que vous pouvez vous connecter et créer dans les tablespaces USERS avec un quota de 2 Mo.



```SQL
SQL> ALTER SESSION SET "_ORACLE_SCRIPT"=true;

Session altered.

SQL> CREATE USER simazenoux IDENTIFIED BY password DEFAULT TABLESPACE users QUOTA 2M ON users;

User created.

SQL> GRANT connect, resource TO simazenoux;

Grant succeeded.

SQL> connect simazenoux/password
Connected.
```

### Créer un utilisateur kay avec le mot de passe mary et avec un quota de 1Mo

```SQL
SQL> CREATE USER kay IDENTIFIED BY mary DEFAULT TABLESPACE users QUOTA 1M ON users;

User created.
```

### En tant qu’admin, copier la table EMP à partir du schéma SCOTT dans le compte de kay.

```SQL
SQL> CREATE TABLE kay.emp AS SELECT * FROM scott.emp;

Table created.
```

### A partir du dictionnaire de données, afficher les informations sur vous et kay

```SQL
SQL> SELECT * FROM dba_users WHERE username IN ('simazenoux','kay');

no rows selected
```


### En tant qu’administrateur, supprimer le quota de kay

```SQL
SQL> ALTER USER kay QUOTA UNLIMITED ON users;

User altered.
```


### Supprimer le compte de kay


```SQL
DROP USER kay CASCADE;
```


### Vous avez oublié le mot de passe pour vous. Allouer à votre compte le mot de passe olink et faire en sorte que Oracle vous demande de changer votre mot de passe à la prochaine connexion

```SQL
SQL> ALTER USER simazenoux IDENTIFIED BY oling PASSWORD EXPIRE;

User altered.
```

## Gestion des profils


```SQL
SQL> CREATE PROFILE max_two_sessions LIMIT SESSIONS_PER_USER 2;

Profile created.

SQL> ALTER USER simazenoux PROFILE max_two_sessions;

User altered.
```

À la troisième connexion consécutive, on obtient le message suivant : 

```console
sqlplus simazenoux/oling

SQL*Plus: Release 18.0.0.0.0 - Production on Fri Mar 31 21:33:02 2023
Version 18.3.0.0.0

Copyright (c) 1982, 2018, Oracle.  All rights reserved.

ERROR:
ORA-02391: exceeded simultaneous SESSIONS_PER_USER limit

``` 


## Gestion des privilèges

### En tant que scott, accorder à votre compte la possibilité de sélectionner des données dans la table EMP de scott. Tester le avec votre compte ( faire « select * from scott.emp » avec votre compte)

```SQL
SQL> GRANT SELECT ON scott.emp TO simazenoux WITH GRANT OPTION;
```

Depuis une session simazenoux :

```SQL
SQL> SELECT * FROM scott.emp;

EMPNO   ENAME   JOB	       MGR    HIREDATE    SAL    COMM   DEPTNO
-----   -----   --------   ----   ---------   ----   ----   ------
7369    SMITH   CLERK	   7902   17-DEC-80    800          20
7499    ALLEN   SALESMAN   7698   20-FEB-81   1600    300   30
7521    WARD    SALESMAN   7698   22-FEB-81   1250    500   30
7566    JONES   MANAGER	   7839   02-APR-81   2975     20
7567    DOE	    SALESMAN   7521   29-APR-81	  1975    100   30
7568    TOTO    SALESMAN   7521   29-APR-81	  1975    100   30
``` 

###  Connecter-vous en tant que scott et accorder à votre compte la possibilité de sélectionner des données dans la table EMP de scott en donnant la capacité pour vous d'accorder aux autres utilisateurs la possibilité de sélection de cette table (table EMP de scott)

```SQL
[simazenoux@oracle-simazenoux ~]$ sqlplus scott/tiger

SQL*Plus: Release 18.0.0.0.0 - Production on Fri Mar 31 21:45:37 2023
Version 18.3.0.0.0

Copyright (c) 1982, 2018, Oracle.  All rights reserved.

Last Successful login time: Tue Mar 14 2023 17:18:14 +02:00

Connected to:
Oracle Database 18c Enterprise Edition Release 18.0.0.0.0 - Production
Version 18.3.0.0.0

SQL> GRANT SELECT ON emp TO simazenoux WITH GRANT OPTION;

Grant succeeded.

```

### Examiner les vues du dictionnaire de données qui enregistrent ces informations

```SQL
SQL> SELECT * FROM user_tab_privs WHERE table_name='emp';

no rows selected
```

### Recréer l'utilisateur kay et donner-lui la possibilité de se connecter à la base de données

```SQL
SQL> CREATE USER kay IDENTIFIED BY mary;

User created.

SQL> GRANT CREATE SESSION TO kay;

Grant succeeded.
```

### Avec votre compte, donner le droit de sélectionner la table EMP de Scott à kay. Pour tester, en tant que kay, interroger la table EMP de Scott


Depuis simazenoux;
```SQL
SQL> GRANT SELECT ON scott.emp TO kay;

Grant succeeded.
```

Depuis kay:
```SQL
SQL> SELECT * FROM scott.emp;

EMPNO   ENAME   JOB	       MGR    HIREDATE    SAL    COMM   DEPTNO
-----   -----   --------   ----   ---------   ----   ----   ------
7369    SMITH   CLERK	   7902   17-DEC-80    800          20
7499    ALLEN   SALESMAN   7698   20-FEB-81   1600    300   30
7521    WARD    SALESMAN   7698   22-FEB-81   1250    500   30
7566    JONES   MANAGER	   7839   02-APR-81   2975     20
7567    DOE	    SALESMAN   7521   29-APR-81	  1975    100   30
7568    TOTO    SALESMAN   7521   29-APR-81	  1975    100   30
```

Cela fonctionne.


### En tant que Scott, supprimer votre privilège de lecture sur sa table EMP. En tant que kay, interroger la table EMP de Scott. Que se passe- t-il?


Depuis scott:
```SQL
SQL> REVOKE SELECT ON emp FROM simazenoux;

Revoke succeeded.
```


Depuis kay:
```SQL
SQL> SELECT * FROM scott.emp;
SELECT * FROM scott.emp
                    *
ERROR at line 1:
ORA-00942: table or view does not exist
``` 

Les privilèges ont bien été révoqués en cascade.


## Gestion des rôles

