# Lab 1:

Pour se connecter au serveur :
```console
ssh simazenoux@oracle-simazenoux
```

Si le DNS ne fonctionne pas :
```console
ssh simazenoux@10.16.39.47
```

Pour définir le mot de passe de l'utilisateur oracle :
```console
sudo passwd oracle
```

Pour changer d'utilisateur :
```console
su oracle
```

Pour lancer l'instance :
```console
rlwrap sqlplus / as sysdba
```





## 1- Utilisation des outils d'administration en ligne

1. Démarrez l'instance

```console
SQL> startup
```

2. Quelle est la taille de la SGA (zone mémoire globale du système) ?

```console
SQL> show sga

Total System Global Area  771747944 bytes
Fixed Size		    8900712 bytes
Variable Size		  528482304 bytes
Database Buffers	  226492416 bytes
Redo Buffers		    7872512 bytes
```

3. Arrêter l'instance

```console
SQL> shutdown
Database closed.
Database dismounted.
ORACLE instance shut down.
```

3. Redémarrez l'instance

```console
SQL> startup   
ORACLE instance started.

Total System Global Area  771747944 bytes
Fixed Size		    8900712 bytes
Variable Size		  612368384 bytes
Database Buffers	  142606336 bytes
Redo Buffers		    7872512 bytes
Database mounted.
Database opened.

```


## 2- Analyse d'une instance Oracle existante (eg., ORCLCDB)

1. Identifiez le nom de la base de données, le nom de l’instance et la taille des blocs de la base de données
    - Avec des commandes internes
    - Avec des requêtes SQL

Avec des commandes internes :
```console
SQL> show parameter db_name;   

NAME				                 TYPE	     VALUE
------------------------------------ ----------- ------------------------------
db_name 			                 string	     ORCLCDB
```

```console
SQL> show parameter instance_name;

NAME				                 TYPE	     VALUE
------------------------------------ ----------- ------------------------------
instance_name			             string	     ORCLCDB
```

```console
SQL> show parameter db_block_size;

NAME                                 TYPE	     VALUE
------------------------------------ ----------- ------------------------------
db_block_size			             integer	 8192
```




Avec des commandes SQL :
```sql
SQL> SELECT name FROM v$database;

NAME
---------
ORCLCDB
```

```sql
SQL> SELECT instance_name FROM v$instance;

INSTANCE_NAME
----------------
ORCLCDB
```

```sql
SQL> SELECT block_size FROM dba_tablespaces WHERE rownum = 1;

BLOCK_SIZE
----------
      8192
```



2. Énumérez le nom et la taille des fichiers de données, des fichiers de reprises et le nom des fichiers de contrôle


Pour afficher le nom et la taille des fichiers de données :
```sql
SQL> select name, bytes from v$datafile;

NAME                                               BYTES
-------------------------------------------------- ----------
/opt/oracle/oradata/ORCLCDB/system01.dbf            901775360
/opt/oracle/oradata/ORCLCDB/sysaux01.dbf           1195376640
/opt/oracle/oradata/ORCLCDB/undotbs01.dbf           325058560
/opt/oracle/oradata/ORCLCDB/pdbseed/system01.dbf    283115520
/opt/oracle/oradata/ORCLCDB/pdbseed/sysaux01.dbf    377487360
/opt/oracle/oradata/ORCLCDB/users01.dbf               5242880
/opt/oracle/oradata/ORCLCDB/pdbseed/undotbs01.dbf   104857600
/opt/oracle/oradata/ORCLCDB/ORCLPDB1/system01.dbf   283115520
/opt/oracle/oradata/ORCLCDB/ORCLPDB1/sysaux01.dbf   387973120
/opt/oracle/oradata/ORCLCDB/ORCLPDB1/undotbs01.dbf  104857600
/opt/oracle/oradata/ORCLCDB/ORCLPDB1/users01.dbf      5242880
```

Pour afficher le nom et la taille des fichiers de reprise :
```sql
SQL> SELECT members, bytes FROM v$log;

   MEMBERS      BYTES
---------- ----------
         1  209715200
         1  209715200
         1  209715200
```

Pour afficher le nom des fichiers de contrôle :
```sql
SQL> SELECT name FROM v$controlfile;

NAME
--------------------------------------------------------------------------------
/opt/oracle/oradata/ORCLCDB/control01.ctl
/opt/oracle/oradata/ORCLCDB/control02.ctl
```

3. Tentez de modifier la taille du bloc de la base de données, i.e. arrêter la base de données, créer votre fichier pfile en copiant un fichier pfile existant
(/opt/oracle/admin/ORACLCDB/pfile/ ), modifier le paramètre db_block_size (doubler
sa taille) dans votre pfile, redémarrer la base de données (startup pfile = ...). Que se passe-t-il ?

On arrète la base de données :
```sql
SQL> shutdown
Database closed.
Database dismounted.
ORACLE instance shut down.
```

On créé notre copie du fichier pfile existant :
```console
cp /opt/oracle/admin/ORCLCDB/pfile/init.ora.922019114844 /opt/oracle/admin/ORCLCDB/pfile/init-copy.ora
```

On modifie alors notre copie pour spéficier la taille des blocks de 8096 à 16192 :
```console
diff /opt/oracle/admin/ORCLCDB/pfile/init.ora.922019114844 /opt/oracle/admin/ORCLCDB/pfile/init-copy.ora
8c8
< db_block_size=8192
---
> db_block_size=16384
```

On essaye désormais de redémarrer l'instance en spécifiant notre fichier pfile modifié :
```SQL
SQL> startup pfile='/opt/oracle/admin/ORCLCDB/pfile/init-copy.ora'
ORACLE instance started.

Total System Global Area  771747944 bytes
Fixed Size                  8900712 bytes
Variable Size             612368384 bytes
Database Buffers          142606336 bytes
Redo Buffers                7872512 bytes
ORA-00058: DB_BLOCK_SIZE must be 8192 to mount this database (not 16384)
```
4. Exécuter le fichier script pour créer le user scott (password : tiger) et sa table emp sur le site www.isima.fr/~kang/adminoracleF3/create_scott.sql.Connectez-vous sous scott/tiger et insérez une ligne dans la table EMP (ex. insert into emp(empno, ename) values ( ....,’toto’);. Ouvrez une seconde session et essayez d’arrêter la base de données en mode normal. Que se passe-il ? Pourquoi on ne peut pas arrêter le serveur ? Ouvrez une autre session en tant qu’administrateur. Essayer la même chose en mode shutdown immediate, expliquez ce qu’il se passe. Rédémarrer l’instance. Ouvrez une nouvelle session sqlplus en tant que scott/tiger et vérifier ce qui s’est passé avec l’employé ’toto’ que vous avez inséré précédemment.

```sql
ALTER session set "_ORACLE_SCRIPT"=true;
CREATE user scott identified by tiger default tablespace users quota 1M on users;
GRANT CONNECT, resource to scott;

CONNECT scott/tiger;

CREATE TABLE EMP(
    EMPNO NUMBER(4) NOT NULL primary key,
    ENAME VARCHAR2(10),
    JOB VARCHAR2(9),
    MGR NUMBER(4),
    HIREDATE DATE,
    SAL NUMBER(7, 2),
    COMM NUMBER(7, 2),
    DEPTNO NUMBER(2)
);
		
INSERT INTO emp VALUES
(7369,'SMITH','CLERK',7902,to_date('17-12-1980','dd-mm-yyyy'),800,NULL,20);

INSERT INTO emp VALUES
(7499,'ALLEN','SALESMAN',7698,to_date('20-2-1981','dd-mm-yyyy'),1600,300,30);

INSERT INTO emp VALUES
(7521,'WARD','SALESMAN',7698,to_date('22-2-1981','dd-mm-yyyy'),1250,500,30);

INSERT INTO emp VALUES
(7566,'JONES','MANAGER',7839,to_date('2-4-1981','dd-mm-yyyy'),2975,NULL,20);

		
COMMIT;
```

Connexion en tant qu'utilisateur scott :
```console
rlwrap sqlplus scott
```


Insertion d'une ligne dans la table EMP :
```SQL
SQL> INSERT INTO emp VALUES (7567,'DOE','SALESMAN',7521,to_date('29-4-1981','dd-mm-yyyy'),1975,100,30);
```

Tentative d'arrêt de la base en tant qu'utilisateur scott :
```SQL
SQL> shutdown
ORA-01031: insufficient privileges
```

Tentative d'arrêt de la base en tant qu'administrateur :
```SQL
SQL> shutdown;
ORA-01097: cannot shutdown while in a transaction - commit or rollback first
```

Tentative d'arrêt immédiat de la base en tant qu'utilisateur oracle :
```SQL
SQL> shutdown immediate
Database closed.
Database dismounted.
ORACLE instance shut down.
```

On redémarre alors la base en tant qu'utilisateur oracle :
```SQL
QL> startup
ORACLE instance started.

Total System Global Area  771747944 bytes
Fixed Size                  8900712 bytes
Variable Size             633339904 bytes
Database Buffers          121634816 bytes
Redo Buffers                7872512 bytes
Database mounted.
Database opened.
```

On se connecte alors en tant qu'utilisateur scott pour vérifier si notre ligne a bien été insérée :
```SQL
SQL> SELECT empno FROM emp WHERE ename='DOE';

     EMPNO
----------
      7567
```



5. Assurez-vous qu’il y a au moins deux sessions ouvertes, une session en tant
qu’utilisateur scott et une autre en tant qu’admin.

    - A partir de la session scott, insérer une ligne dans la table EMP (insert into emp(empno, ename) values ( ....,’toto’) ;
    - A partir de la session admin, arrêter l’instance en mode transactionnel. Que se passe-t-il ?
    - Ouvrir une nouvelle fenêtre shell et essayez de vous connectez en tant que scott/tiger. Que se passe-t-il ?
    - Revenir à la session scott à partir de laquelle vous avez exécuté l’insertion de l’employé ’toto’. Valider la transaction. Que se passe-t-il au niveau de la session admin partir de laquelle vous avez exécuté le shutdown transactionnel ?

```SQL
SQL> INSERT INTO emp VALUES (7568,'TOTO','SALESMAN',7521,to_date('29-4-1981','dd-mm-yyyy'),1975,100,30);

1 row created.
```

```SQL
SQL> shutdown transactional
```
La commande reste active mais rien ne se passe pour le moment

```console
rlwrap sqlplus scott/tiger

SQL*Plus: Release 18.0.0.0.0 - Production on Tue Mar 7 16:10:51 2023
Version 18.3.0.0.0

Copyright (c) 1982, 2018, Oracle.  All rights reserved.

ERROR:
ORA-01089: immediate shutdown or close in progress - no operations are
permitted
Process ID: 0
Session ID: 0 Serial number: 0
```

On commit en tant qu'utilisateur scott :
```SQL
SQL> COMMIT;

Commit complete.
```

Après le commit, la commande shutdown transactional conclut :
```SQL
SQL> shutdown transactional
Database closed.
Database dismounted.
ORACLE instance shut down.
```

## 3- Mise à jour du fichier de contrôle

1. Où sont placés les fichiers de contrôle existants et quels sont leurs noms ?


```console
ls /opt/oracle/oradata/ORCLCDB/
control01.ctl  ORCLPDB1/      redo01.log     redo03.log     system01.dbf   undotbs01.dbf
control02.ctl  pdbseed/       redo02.log     sysaux01.dbf   temp01.dbf     users01.dbf
```
2. Multiplexez vos fichiers de contrôle. Créer un nouveau fichier de contrôle ctrl02Test.ctl dans le même répertoire que les fichiers de contrôle existant. Vérifiez si tous les fichiers de contrôle (y compris le nouveau fichier ajouté) sont utilisés.

On s'assure que la base est bien arrêtée.
```SQL
SQL> shutdown
Database closed.
Database dismounted.
ORACLE instance shut down.
```

On renomme un des fichiers de controle.
```console
mv /opt/oracle/oradata/ORCLCDB/control01.ctl /opt/oracle/oradata/ORCLCDB/control00.ctl
```

On essaye alors de redémarrer l'instance, sans succès.
```SQL
SQL> startup
ORACLE instance started.

Total System Global Area  771747944 bytes
Fixed Size                  8900712 bytes
Variable Size             633339904 bytes
Database Buffers          121634816 bytes
Redo Buffers                7872512 bytes
ORA-00205: error in identifying control file, check alert log for more info
```

On redonne au fichier de controle son nom original
```console
mv /opt/oracle/oradata/ORCLCDB/control00.ctl /opt/oracle/oradata/ORCLCDB/control01.ctl
```

On vérifie alors que l'instance redémarre.
```sql
SQL> startup
ORACLE instance started.

Total System Global Area  771747944 bytes
Fixed Size                  8900712 bytes
Variable Size             633339904 bytes
Database Buffers          121634816 bytes
Redo Buffers                7872512 bytes
Database mounted.
Database opened.
```


3. Multiplexez vos fichiers de contrôle. Créer un nouveau fichier de contrôle ```ctrl02Test.ctl``` dans le même répertoire que les fichiers de contrôle existant. Vérifiez si tous les fichiers de contrôle (y compris le nouveau fichier ajouté) sont utilisés

On éteind la base :
```SQL
SQL> shutdown
Database closed.
Database dismounted.
ORACLE instance shut down.
```

On créé notre nouveau fichier de contrôle à partir d'un des fichiers de contrôle existant.
```console
cp /opt/oracle/oradata/ORCLCDB/control02.ctl /opt/oracle/oradata/ORCLCDB/ctrl02Test.ctl
```

On modifie notre fichier ```init.ora``` pour y inclure notre nouveau fichier de contrôle.
```
###########################################
# File Configuration
###########################################
control_files=("/opt/oracle/oradata/ORCLCDB/control01.ctl", "/opt/oracle/oradata/ORCLCDB/control02.ctl", "/opt/oracle/oradata/ORCLCDB/ctrl02Test.ctl")
```



On redémmare la base en précisant notre pfile:
```SQL
SQL> startup pfile='/opt/oracle/admin/ORCLCDB/pfile/init.ora.922019114844'
ORACLE instance started.

Total System Global Area  771747944 bytes
Fixed Size                  8900712 bytes
Variable Size             612368384 bytes
Database Buffers          142606336 bytes
Redo Buffers                7872512 bytes
Database mounted.
Database opened.
```

On vérifie alors que notre nouveau fichier apparaît bien.
```SQL
SQL> SELECT name FROM v$controlfile;

NAME
--------------------------------------------------------------------------------
/opt/oracle/oradata/ORCLCDB/control01.ctl
/opt/oracle/oradata/ORCLCDB/control02.ctl
/opt/oracle/oradata/ORCLCDB/ctrl02Test.ctl
```


4. Quel est le nombre maximum de fichiers de données que vous pouvez créer dans
la base de données ?

```SQL
SQL> SELECT records_total FROM v$controlfile_record_section WHERE type='DATAFILE';

RECORDS_TOTAL
-------------
         1024
```

La base peut donc contenir jusqu'à 1024 fichiers de données.


## 4- Mise à jour des fichiers de reprise

1. Énumérez le nombre et l’emplacement des fichiers de reprise existants. Affichez le nombre de groupes de fichiers de reprise et de membres que votre base de données contient.


Pour connaître l'emplacement des fichiers de reprise existants :
```SQL
SQL> SELECT member FROM v$logfile;

MEMBER
--------------------------------------------------------------------------------
/opt/oracle/oradata/ORCLCDB/redo03.log
/opt/oracle/oradata/ORCLCDB/redo02.log
/opt/oracle/oradata/ORCLCDB/redo01.log
```

Pour connaître les groupes des fichiers de reprise et les membres:
```SQL
SQL> SELECT group#, members FROM v$log;

    GROUP#    MEMBERS
---------- ----------
         1          1
         2          1
         3          1
```

2. Dans quel mode de base de données votre base est-elle configurée ? L’archivage est-il activé ?


```SQL
SQL> SELECT log_mode FROM v$database;

LOG_MODE
------------
NOARCHIVELOG
```

L'archivage n'est donc pas activé.

3. Ajoutez un membre de reprise à chaque groupe dans votre base de données. Le faire dans le même répertoire avec les conventions suivantes : si le groupe 1 possède un fichier rlog1a.log, ajoutez un membre rlog1b.log. Vérifiez le résultat.

```SQL
SQL> ALTER DATABASE ADD LOGFILE MEMBER '/opt/oracle/oradata/ORCLCDB/redo1b.log' TO GROUP 1, '/opt/oracle/oradata/ORCLCDB/redo2b.log' TO GROUP 2, '/opt/oracle/oradata/ORCLCDB/redo3b.log' TO GROUP 3;

Database altered.
```


On peut alors vérifier que ces fichiers ont bien été créés.
```SQL
SQL> SELECT member FROM v$logfile;

MEMBER
--------------------------------------------------------------------------------
/opt/oracle/oradata/ORCLCDB/redo03.log
/opt/oracle/oradata/ORCLCDB/redo02.log
/opt/oracle/oradata/ORCLCDB/redo01.log
/opt/oracle/oradata/ORCLCDB/redo1b.log
/opt/oracle/oradata/ORCLCDB/redo2b.log
/opt/oracle/oradata/ORCLCDB/redo3b.log

6 rows selected.
```


4. Créez un nouveau groupe de fichiers de reprise avec 2 membres dans le répertoire DISK4 et vérifiez son existence.

On créé notre répertoire ```DISK4```.
```console
mkdir /opt/oracle/oradata/ORCLCDB/DISK4/
```


```sql
SQL> ALTER DATABASE ADD LOGFILE GROUP 4 ('/opt/oracle/oradata/ORCLCDB/DISK4/redo01c.log', '/opt/oracle/oradata/ORCLCDB/DISK4/redo02c.log') SIZE 100M;

Database altered.
```

```sql
SQL> SELECT group#, members FROM v$log;

    GROUP#    MEMBERS
---------- ----------
         1          2
         2          2
         3          2
         4          2
```

5. Supprimez le groupe de reprise de la question 4.

```sql
SQL> ALTER DATABASE DROP logfile GROUP 4;

Database altered.
```

```sql
SQL> SELECT group#, members FROM v$log;

    GROUP#    MEMBERS
---------- ----------
         1          2
         2          2
         3          2
```