# Lab 2

## Gestion des fichiers de données

### Créez des tablespaces avec les noms et le type de stockage suivants 

On crée dans les répertoires:
``` console
mkdir /opt/oracle/oradata/ORCLCDB/DISK1/
mkdir /opt/oracle/oradata/ORCLCDB/DISK3/
mkdir /opt/oracle/oradata/ORCLCDB/DISK4/
```

``` sql
SQL> CREATE TABLESPACE DATAsimazenoux DATAFILE '/opt/oracle/oradata/ORCLCDB/DISK4/datasimazenoux.dbf' SIZE 20 M;

Tablespace created.
```

``` sql
SQL> CREATE TEMPORARY TABLESPACE TEMPsimazenoux TEMPFILE '/opt/oracle/oradata/ORCLCDB/DISK3/tempsimazenoux.dbf' SIZE 5M;

Tablespace created.

DROP TABLESPACE INDXsimazenoux
``` 

``` sql
SQL> CREATE TABLESPACE INDXsimazenoux DATAFILE '/opt/oracle/oradata/ORCLCDB/DISK3/indxsimazenoux.dbf' SIZE 5M AUTOEXTEND ON NEXT 500K;

Tablespace created.
```

``` sql
SQL> CREATE TABLESPACE RONLYsimazenoux DATAFILE '/opt/oracle/oradata/ORCLCDB/DISK1/ronlysimazenoux.dbf' SIZE 5M;

Tablespace created.

SQL> ALTER TABLESPACE RONLYsimazenoux READ ONLY;

Tablespace altered.
``` 

``` sql
SQL> SELECT tablespace_name, file_name, bytes/1024/1024 AS size_mb FROM dba_data_files;

TABLESPACE_NAME   FILE_NAME                                               SIZE_MB
---------------   -----------------------------------------------------   -------
SYSTEM            /opt/oracle/oradata/ORCLCDB/system01.dbf                860
SYSAUX            /opt/oracle/oradata/ORCLCDB/sysaux01.dbf                1150
UNDOTBS1          /opt/oracle/oradata/ORCLCDB/undotbs01.dbf               310
USERS             /opt/oracle/oradata/ORCLCDB/users01.dbf                 5
DATASIMAZENOUX    /opt/oracle/oradata/ORCLCDB/DISK4/datasimazenoux.dbf    20
INDXSIMAZENOUX    /opt/oracle/oradata/ORCLCDB/DISK3/indxsimazenoux.dbf    5
RONLYSIMAZENOUX   /opt/oracle/oradata/ORCLCDB/DISK1/ronlysimazenoux.dbf   5
```


``` sql
SQL> SELECT tablespace_name, file_name, bytes/1024/1024 AS size_mb FROM dba_temp_files;

TABLESPACE_NAME   FILE_NAME                                              SIZE_MB
---------------   ----------------------------------------------------   -------
TEMPSIMAZENOUX    /opt/oracle/oradata/ORCLCDB/DISK3/tempsimazenoux.dbf   5
TEMP              /opt/oracle/oradata/ORCLCDB/temp01.dbf                 130
```


### Donner la liste des tablespaces par défaut selon des utilisateurs

``` sql
SQL> SELECT username, default_tablespace FROM dba_users;

USERNAME                 DEFAULT_TABLESPACE
----------------------   ------------------
SYS                      SYSTEM
SYSTEM                   SYSTEM
XS$NULL                  SYSTEM
OJVMSYS                  SYSTEM
LBACSYS                  SYSTEM
OUTLN                    SYSTEM
SYS$UMF                  SYSTEM
DBSNMP                   SYSAUX
APPQOSSYS                SYSAUX
DBSFWUSER                SYSAUX
GGSYS                    SYSAUX
ANONYMOUS                SYSAUX
CTXSYS                   SYSAUX
DVF                      SYSAUX
SI_INFORMTN_SCHEMA       SYSAUX
DVSYS                    SYSAUX
GSMADMIN_INTERNAL        SYSAUX
ORDPLUGINS               SYSAUX
MDSYS                    SYSAUX
OLAPSYS                  SYSAUX
ORDDATA                  SYSAUX
XDB                      SYSAUX
WMSYS                    SYSAUX
ORDSYS                   SYSAUX
GSMCATUSER               USERS
MDDATA                   USERS
SYSBACKUP                USERS
REMOTE_SCHEDULER_AGENT   USERS
GSMUSER                  USERS
SYSRAC                   USERS
AUDSYS                   USERS
DIP                      USERS
SYSKM                    USERS
ORACLE_OCM               USERS
SCOTT                    USERS
SYSDG                    USERS
```


### Quelles sont les deux méthodes pour augmenter la taille d’un tablespace ? Tester ces deux méthodes pour allouer 500Ko supplémentaires au tablespace DATAToto. Vérifiez si la taille du tablespace a été bien augmentée après avoir réalisé chaque méthode

Pour redimensionner le datafile :
``` sql
SQL> ALTER DATABASE DATAFILE '/opt/oracle/oradata/ORCLCDB/DISK4/datasimazenoux.dbf' RESIZE 20500K;

Database altered.
```

Pour ajouter un datafile au tablespace :
``` sql
SQL> ALTER TABLESPACE DATAsimazenoux add DATAFILE '/opt/oracle/oradata/ORCLCDB/DISK4/Datasimazenouxb.dbf' SIZE 500K;

Tablespace altered.
```

``` SQL
SQL> SELECT file_name, bytes FROM dba_data_files WHERE tablespace_name='DATASIMAZENOUX';

FILE_NAME                                               BYTES
-----------------------------------------------------   ------------------------
/opt/oracle/oradata/ORCLCDB/DISK4/datasimazenoux.dbf    20996096
/opt/oracle/oradata/ORCLCDB/DISK4/Datasimazenouxb.dbf   516096
```








### Déplacez le tablespace INDXToto dans le répertoire DISK1


``` SQL
SQL> ALTER DATABASE MOVE DATAFILE '/opt/oracle/oradata/ORCLCDB/DISK3/indxsimazenoux.dbf' TO '/opt/oracle/oradata/ORCLCDB/DISK1/indxsimazenoux.dbf';

Database altered.
```

``` SQL
SQL> SELECT file_name FROM dba_data_files WHERE tablespace_name='INDXSIMAZENOUX';

FILE_NAME
----------------------------------------------------
/opt/oracle/oradata/ORCLCDB/DISK1/indxsimazenoux.dbf
```



### Mettre le tablespace RONLYToto en mode read write et créer une table dont le nom est votre login. Remettre ce tablespace en mode read only et tenter de créer une table supplémentaire. Que se passe-il ? Supprimer la table créée. Que se passe-t-il ?

Tablespace RONLYsimazenoux en mode read-write.
``` SQL
SQL> ALTER TABLESPACE RONLYsimazenoux READ WRITE;

Tablespace altered.
```


``` SQL
SQL> CREATE TABLE simazenoux (firstname VARCHAR(20), lastname VARCHAR(20)) TABLESPACE RONLYsimazenoux;


Table created.
```

Tablespace RONLYsimazenoux en mode read-only.
``` SQL
SQL> ALTER TABLESPACE RONLYsimazenoux READ ONLY;

Tablespace altered.
```

On ne peut désormais plus créer de table.
``` SQL
SQL> CREATE TABLE simazenoux2 (firstname VARCHAR(20), lastname VARCHAR(20)) TABLESPACE RONLYsimazenoux;
CREATE TABLE simazenoux2 (firstname VARCHAR(20), lastname VARCHAR(20)) TABLESPACE RONLYsimazenoux
*
ERROR at line 1:
ORA-01647: tablespace 'RONLYSIMAZENOUX' is read-only, cannot allocate space in it
```

On arrive cependant à supprimer la table même si le TABLESPACE est en read-only.
``` SQL
SQL> DROP TABLE simazenoux;

Table dropped.
```


### Supprimer le tablespace RONLYToto et vérifiez l’opération. Est-ce que les fichiers associés ont été supprimés physiquement ?


``` SQL
SQL> DROP TABLESPACE RONLYsimazenoux;

Tablespace dropped.
```


# TODO : add to the report





## 2 - Segments


### Identifier les différents types de segments dans la base de donnée

```SQL
SQL> SELECT DISTINCT segment_type FROM dba_segments;

SEGMENT_TYPE
------------------
INDEX
CLUSTER
TABLE PARTITION
LOBINDEX
TABLE SUBPARTITION
SYSTEM STATISTICS
LOBSEGMENT
INDEX PARTITION
ROLLBACK
TABLE
LOB PARTITION
NESTED TABLE
TYPE2 UNDO

```



### Quel est le fichier ayant de l’espace alloué pour la table EMP ?


```SQL
SQL> SELECT DISTINCT file_name FROM dba_extents 
     JOIN dba_data_files ON dba_extents.file_id = dba_data_files.file_id 
     WHERE segment_name = 'EMP';

FILE_NAME
--------------------------------------------------------------------------------
/opt/oracle/oradata/ORCLCDB/users01.dbf

```
